/**
 * Copyright 1993-2015 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

#include <stdio.h>
#include <stdlib.h>

// For the CUDA runtime routines (prefixed with "cuda_")
#include <cuda_runtime.h>

#include "helper_cuda.h"

// Helper functions and utilities to work with CUDA
#include <helper_functions.h>

#include <timer.h>
#include <omp.h>

// Valores por defecto
#define ANCHO_A 10
#define ALTO_A 10
#define ANCHO_B 10
#define ALTO_B 10

#define ERROR 0.01f

#define DEBUG 0
#define ONLY_TIME 0

#define REGS_THREAD 26

/**
 * CUDA Kernel Device code
 *
 * Calcula el producto matricial de A y B y devuelve el resultado en C.
 * A ha de tener dimensiones wA*hA
 * B ha de tener dimensiones wB*wA
 * El calculo empieza en la posicion inicio y acaba en la posicion fin
 */
__global__ void productoMatricialGPU(const float* A, const float* B, float* C, const int wA, const int wB, const int hA, const int tileRange, const int inicioPosicion, const int finPosicion){

   extern __shared__ float compartida[];

   float* compartidaA= compartida;
   float* compartidaB= &compartida[tileRange * tileRange];

   const int wBredondeado = (wB%tileRange) ? wB + tileRange - wB%tileRange : wB;
   const int tileInicio = inicioPosicion*wBredondeado/tileRange;
   const int tileFin = finPosicion*wBredondeado/tileRange;
   const int tidb = threadIdx.z * blockDim.y * blockDim.x + threadIdx.y * blockDim.x + threadIdx.x;
   int bidg = blockIdx.z * gridDim.y * gridDim.x + blockIdx.y * gridDim.x + blockIdx.x + tileInicio;
   const int gridSize = gridDim.x * gridDim.y * gridDim.z;
   const int filaThread = tidb/tileRange;
   const int columnaThread = tidb%tileRange;
   int filaTileC;
   int columnaTileC;
   int posA;
   int posB;
   float res;

   if(tidb < tileRange*tileRange){

      for(; bidg<tileFin; bidg+=gridSize){

         filaTileC = bidg*tileRange/wBredondeado*tileRange;
         columnaTileC = bidg*tileRange%wBredondeado;
         posA = (filaTileC + filaThread)*wA + columnaThread;
         posB = columnaTileC + filaThread*wB + columnaThread;
         res = 0.0;

         for(int i=0; i<wA; i+=tileRange){

            compartidaA[tidb]= (columnaThread+i<wA) ? A[posA+i] : 0.0;
            compartidaB[tidb]= (filaThread+i<wA) ? B[posB+i*wB] : 0.0;

            __syncthreads();

            for(int j=0; j<tileRange && i+j<wA; j++)
               res+=compartidaA[filaThread*tileRange+j]*compartidaB[columnaThread+j*tileRange];

            __syncthreads();
         }

         if(columnaTileC+columnaThread<wB && filaTileC+filaThread<hA)
            C[(filaTileC + filaThread)*wB + columnaTileC + columnaThread]=res;
      }
   }
}

//Calcula el producto matricial de A y B y devuelve el resultado en C
//A ha de tener dimensiones wA*hA
//B ha de tener dimensiones wB*wA
//El calculo empieza en la posicion 0 y acaba en la posicion fin
void productoMatricialCPU(const float* A, const float* B, float* C, const int wA, const int wB, const int hA, const int tileRange, const int finPosicion, const int numHilos){

   float cumsum;
   const int fin= (hA<finPosicion*tileRange) ? hA : finPosicion*tileRange;

   omp_set_dynamic(0);

   #pragma omp parallel for private(cumsum) num_threads(numHilos)
   for(int i=0; i<fin; i++)
      for(int j=0; j<wB; j++){
         cumsum=0.0;
         for(int k=0; k<wA; k++)
            cumsum+=A[i*wA+k]*B[k*wB+j];
         C[i*wB+j]=cumsum;
      }
}

// Rutina para finalizar la ejecucion
void finalizarAplicacion(int estado){

   cudaError_t error=cudaDeviceReset();

   if(error != cudaSuccess){

      fprintf(stderr, "Fallo al reiniciar el dispositivo: %s\n", cudaGetErrorString(error));
      exit(EXIT_FAILURE);
   }

   #if ONLY_TIME==0
      if(estado==EXIT_SUCCESS) printf("Ejecución correcta\n");
   #endif

   exit(estado);
}

// Rutina para inicializar cada elemento de una matriz dada con valores aleatorios entre -1 y 1
void inicializarMatriz(float* A, int n){

   for(int i=0; i<n; i++) A[i]=-1+(double) rand()/(double) RAND_MAX * 2;
}

void comprobarMatriz(const float* A, const float* B, const float* C, const int wA, const int wB, const int hA){

   const int totalSize=wB*hA;

   float* Caux= (float*) malloc(totalSize*sizeof(float));

   if(Caux==NULL){

      fprintf(stderr, "Fallo al reservar memoria en el host\n");
      finalizarAplicacion(EXIT_FAILURE);
   }

   float cumsum;
   #pragma omp for private(cumsum)
   for(int i=0; i<hA; i++){
      for(int j=0; j<wB; j++){
         cumsum=0.0;
         for(int k=0; k<wA; k++)
            cumsum=cumsum+A[i*wA+k]*B[k*wB+j];
         Caux[i*wB+j]=cumsum;
      }
   }

   float diferencia, error;

   for(int i=0; i<totalSize; i++){

      diferencia=C[i]-Caux[i];
      if(diferencia < 0) diferencia=-diferencia;
      error=ERROR*C[i];
      if(error < 0) error=-error;
      if(diferencia>error){
         fprintf(stderr, "Fallo en la verificación, cálculo erróneo en posición %d\n", i);
         fprintf(stderr, "Obtenido %.9f esperado %.9f, es posible que se deba a los límites de precisión del tipo float\n", C[i], Caux[i]);
         finalizarAplicacion(EXIT_FAILURE);
      }
   }

   free(Caux);
}

void versionInvalida(int major, int minor){

   fprintf(stderr,"Dispositivo con número de versión %d.%d no soportado\n", major, minor);
   finalizarAplicacion(EXIT_FAILURE);
}

void compruebaVersion(int major, int minor){

   switch(major){

      case 2: ; if(minor==0 || minor==1) return;
         break;
      case 3: ; if(minor==0 || minor==2 || minor == 5 || minor == 7) return;
         break;
      case 5: ; if(minor==0 || minor==2 || minor == 3) return;
         break;
      case 6: ; if(minor==0 || minor==1 || minor == 2) return;
   }

  versionInvalida(major, minor);
}

int getTBPMP(int major){

   switch(major){

      case 2: return 8;
      case 3: return 16;
      default: return 32;
   }
}

int getRAUS(int major){

   switch(major){

      case 2: return 64;
      default: return 256;
   }
}

int getWAG(int major, int minor){

   switch(major){

      case 2: return 2;
      case 6: ; if(minor==0) return 2;
      default: return 4;
   } 
}

int getRPT(int major, int minor){

   switch(major){

      case 2: return 63;
      case 3: ; if(minor==0) return 63;
      default: return 255;
   } 
}

int getRPMP(int major, int minor){

   switch(major){

      case 2: return 32768;
      case 3: ; if(minor==7) return 131072;
      default: return 65536;
   } 
}

int getSMAUS(int major){

   switch(major){

      case 2: return 128;
      default: return 256;
   }
}

int getSMPMP(int major, int minor){

   switch(major){

      case 2:
      case 3: ; if(minor==7) return 114688;
         return 49152;
      case 5: ; if(minor==2) return 98304;
         break;
      case 6: ; if(minor==1) return 98304;
   }
   return 65536;
}

int minInt(int a, int b){

   if(a<0 || b<a) return b;
   return a;
}

int maxInt(int a, int b){

   if(a<b) return b;
   return a;
}

void autoTuneBlockSize(int* tileRange, int numDispositivos){

   cudaError_t error = cudaSuccess;
   cudaDeviceProp deviceProp;
   int RPT=REGS_THREAD; //26 registersPerThread
   int maxTPB=-1; //1024 threadsPerBlock
   int maxTPMP=-1; //1536 threadsPerMultiProcessor
   int maxRPTB=-1; //32768 registersPerThreadBlock
   int TPW=32; //32 threadsPerWarp
   int maxMSMPB=49152; // maxSharedMemoryPerBlock
   int major; //2.0
   int minor; //2.0
   int maxTBPMP=-1; //8 threadsBlocksPerMultiProcessor
   int maxWPMP=-1; //48 warpsPerMultiProcessor
   int minRAUS=-1; //64 registerAllocationUnitSize
   int minWAG=-1; //2 warpAllocationGranularity
   int maxRPT=-1; //63 registersPerThread
   int maxRPMP=-1; //32768 registersPerMultiProcessor
   int minSMAUS=-1; //128 sharedMemoryAllocUnitSize
   int maxSMPMP=-1; //49152 sharedMemoryPerMultiProcessor

   for(int i=0; i<numDispositivos; i++){

      error=cudaSetDevice(i);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al establecer dispositivo %d: %s\n", i, cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      cudaGetDeviceProperties(&deviceProp, i);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al obtener propiedades del dispositivo %d: %s\n", i, cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      maxTPB=minInt(maxTPB, deviceProp.maxThreadsPerBlock);
      maxTPMP=minInt(maxTPMP, deviceProp.maxThreadsPerMultiProcessor);
      maxRPTB=minInt(maxRPTB, deviceProp.regsPerBlock);
      major=deviceProp.major;
      minor=deviceProp.minor;
      compruebaVersion(major, minor);
      maxTBPMP=minInt(maxTBPMP, getTBPMP(major));
      minRAUS=maxInt(minRAUS, getRAUS(major));
      minWAG=maxInt(minWAG, getWAG(major, minor));
      maxRPT=minInt(maxRPT, getRPT(major, minor));
      maxRPMP=minInt(maxRPMP, getRPMP(major, minor));
      minSMAUS=maxInt(minSMAUS, getSMAUS(major));
      maxSMPMP=minInt(maxSMPMP, getSMPMP(major, minor));
   }

   maxWPMP=maxTPMP/TPW;

   int limite1, limite2, limite3, warpsBloque, aux2;
   int maxLimite=0;
   int aux=1;

   for(int i=1; aux<=minInt(maxTPMP,maxTPB); i++, aux=i*i){

      warpsBloque=(aux+TPW-1)/TPW;
      limite1=minInt(maxTBPMP,maxWPMP/warpsBloque);
      if(RPT>maxRPT) limite2=0;
      else if(warpsBloque>0){

         limite2=RPT*TPW;
         aux2=limite2%minRAUS;
         if(aux2) limite2+=minRAUS-aux2;
         limite2=maxRPTB/limite2;
         limite2-=limite2%minWAG;
         limite2=limite2/warpsBloque*(maxRPMP/maxRPTB);
      }
      else limite2=maxTBPMP;
      limite3=aux*2*sizeof(float);
      aux2=limite3%minSMAUS;
      if(aux2) limite3+=minSMAUS-aux2;
      if(limite3>maxMSMPB) limite3=0;
      else if(limite3>0) limite3=maxSMPMP/limite3;
      else limite3=maxTBPMP;

      limite1=minInt(limite1,limite2);
      limite1=minInt(limite1,limite3);
      limite1*=warpsBloque;

      if(limite1>=maxLimite){
         maxLimite=limite1;
         *tileRange=i;
      }
   }
}

float autoTuneNumHilos(int* numHilos, float* A, float* B, float* C, int wA, int wB, int hA, int tileRange, int fin){

   float tiempoActual;
   float mejorTiempo=-1.0;
   cudaEvent_t eventoInicio, eventoFin;
   cudaError_t error = cudaSuccess;
   int intentosRestantes=16;

   for(int i=1; intentosRestantes>0; i++, intentosRestantes--){

      error=cudaEventCreate(&eventoInicio);
      if(error==cudaSuccess) error=cudaEventCreate(&eventoFin);
      if(error==cudaSuccess) error=cudaEventRecord(eventoInicio,0);
      if(error==cudaSuccess) error=cudaEventSynchronize(eventoInicio);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al tomar tiempo: %s\n", cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      productoMatricialCPU(A, B, C, wA, wB, hA, tileRange, fin, i);

      error=cudaEventRecord(eventoFin,0);
      if(error==cudaSuccess) error=cudaEventSynchronize(eventoFin);
      if(error==cudaSuccess) error=cudaEventElapsedTime(&tiempoActual, eventoInicio, eventoFin);
      if(error==cudaSuccess) error=cudaEventDestroy(eventoInicio);
      if(error==cudaSuccess) error=cudaEventDestroy(eventoFin);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al tomar tiempo: %s\n", cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      if(mejorTiempo<0 || tiempoActual<mejorTiempo){

         mejorTiempo=tiempoActual;
         *numHilos=i;
         intentosRestantes+=i;
      }
   }
   return mejorTiempo;
}

void autoTuneWorkLoad(float* deviceWorkLoad, float* wlGPU, float tiempoCPU, int numDispositivos, dim3 grid, dim3 bloque, size_t memCompartida, float** d_A, float** d_B, float** d_C, int wA, int wB, int hA, int tileRange, int fin){

   cudaEvent_t eventoInicio, eventoFin;
   cudaError_t error = cudaSuccess;

   double* opuestos=(double*) malloc((numDispositivos+1)*sizeof(double));
   double totalOpuestos=0.0;
   float tiempoAux;

   for(int i=0; i<numDispositivos; i++){

      error=cudaSetDevice(i);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al establecer dispositivo %d: %s\n", i, cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      error=cudaEventCreate(&eventoInicio);
      if(error==cudaSuccess) error=cudaEventCreate(&eventoFin);
      if(error==cudaSuccess) error=cudaEventRecord(eventoInicio,0);
      if(error==cudaSuccess) error=cudaEventSynchronize(eventoInicio);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al tomar tiempo: %s\n", cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      productoMatricialGPU<<<grid, bloque, memCompartida>>>(d_A[i], d_B[i], d_C[i], wA, wB, hA, tileRange, 0, fin);

      error=cudaEventRecord(eventoFin,0);
      if(error==cudaSuccess) error=cudaEventSynchronize(eventoFin);
      if(error==cudaSuccess) error=cudaEventElapsedTime(&tiempoAux, eventoInicio, eventoFin);
      if(error==cudaSuccess) error=cudaEventDestroy(eventoInicio);
      if(error==cudaSuccess) error=cudaEventDestroy(eventoFin);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al tomar tiempo: %s\n", cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      opuestos[i]= 1.0/tiempoAux;
      totalOpuestos+=opuestos[i];
   }

   opuestos[numDispositivos]=1.0/tiempoCPU;
   totalOpuestos+=opuestos[numDispositivos];

   totalOpuestos=1.0-opuestos[numDispositivos]/totalOpuestos;
   *deviceWorkLoad=totalOpuestos;
   for(int i=0; i<numDispositivos; i++)
      wlGPU[i]=opuestos[i]/totalOpuestos;

   free(opuestos);
}

int main(int argc, char **argv){

   int wA=ANCHO_A;
   int hA=ALTO_A;
   int wB=ANCHO_B;
   int hB=ALTO_B;
   int bsx;
   int bsy;
   int bsz=1;
   int gsx;
   int gsy;
   int gsz=1;
   float deviceWorkLoad;
   int numHilos;

   // Tratamiento de la entrada
	if(checkCmdLineFlag(argc, (const char **)argv, "help")){

	   printf("Uso: %s [-wA=ancho matriz A] [-hA=alto matriz A] [-wB=ancho matriz B] [-hB=alto matriz B]\n", argv[0]);
   	printf("Todas las variables han de ser enteros estrictamente positivos\n");
   	printf("Valores por defecto: wA=%d, hA=%d, wB=%d, hB=%d\n", wA, hA, wB, hB);
   	finalizarAplicacion(EXIT_SUCCESS);
	}

	if(checkCmdLineFlag(argc, (const char **)argv, "wA")){

      wA=getCmdLineArgumentInt(argc, (const char **)argv, "wA");
	   
      if(wA<=0){

         fprintf(stderr,"wA ha de ser un entero estrictamente positivo\n");
      	finalizarAplicacion(EXIT_FAILURE);
      }
	}

	if(checkCmdLineFlag(argc, (const char **)argv, "hA")){

      hA=getCmdLineArgumentInt(argc, (const char **)argv, "hA");
	   
      if(hA<=0){

         fprintf(stderr,"hA ha de ser un entero estrictamente positivo\n");
      	finalizarAplicacion(EXIT_FAILURE);
      }
	}

	if(checkCmdLineFlag(argc, (const char **)argv, "wB")){

      wB=getCmdLineArgumentInt(argc, (const char **)argv, "wB");
	   
      if(wB<=0){

         fprintf(stderr,"wB ha de ser un entero estrictamente positivo\n");
      	finalizarAplicacion(EXIT_FAILURE);
      }
	}

	if(checkCmdLineFlag(argc, (const char **)argv, "hB")){

      hB=getCmdLineArgumentInt(argc, (const char **)argv, "hB");
	   
      if(hB<=0){

         fprintf(stderr,"hB ha de ser un entero estrictamente positivo\n");
      	finalizarAplicacion(EXIT_FAILURE);
      }
	}

   if(wA!=hB){

      fprintf(stderr,"La anchura de la matriz A y la altura de la matriz B han de ser iguales\n");
      finalizarAplicacion(EXIT_FAILURE);
   }

   float tiempoKernel, tiempoTransferencia1, tiempoTransferencia2;
   cudaEvent_t eventoInicio, eventoFin;
   cudaError_t error = cudaSuccess;

   int numDispositivos = 0;
   error = cudaGetDeviceCount(&numDispositivos);

   if(error!=cudaSuccess){

      fprintf(stderr, "Fallo al obtener número de dispositivos: %s\n", cudaGetErrorString(error));
      finalizarAplicacion(EXIT_FAILURE);
   }

   if(numDispositivos==0){

      printf("No se encontraron dispositivos\n");
      finalizarAplicacion(EXIT_SUCCESS);
   }

   #if ONLY_TIME==0
      printf("Detectados %d dispositivos\n", numDispositivos);
   #endif

   // Reservamos memoria para los vectores en el host
   float* h_A=(float*) malloc(wA*hA*sizeof(float));
   float* h_B=(float*) malloc(wB*hB*sizeof(float));
   float* h_C=(float*) malloc(wB*hA*sizeof(float));
   float** d_A=(float**) malloc(numDispositivos*sizeof(float*));
   float** d_B=(float**) malloc(numDispositivos*sizeof(float*));
   float** d_C=(float**) malloc(numDispositivos*sizeof(float*));
   float* wlGPU=(float*) malloc(numDispositivos*sizeof(float));

   if(h_A==NULL || h_B==NULL || h_C==NULL || d_A==NULL || d_B==NULL || d_C==NULL || wlGPU==NULL){

      fprintf(stderr, "Fallo al reservar memoria en el host\n");
      finalizarAplicacion(EXIT_FAILURE);
   }

   memset(d_A, 0, numDispositivos*sizeof(float*));
   memset(d_B, 0, numDispositivos*sizeof(float*));
   memset(d_C, 0, numDispositivos*sizeof(float*));

   int tileRange=1;
   autoTuneBlockSize(&tileRange, numDispositivos);

   bsx=tileRange;
   bsy=tileRange;

   gsx=(wB+tileRange-1)/tileRange;
   gsy=(hA+tileRange-1)/tileRange;

   #if ONLY_TIME==0
      printf("Inicializando matrices A y B\n");
   #endif
   srand(wA<<hA%wB*hB-bsx+bsy/bsz>>gsz+gsx*gsy);
   inicializarMatriz(h_A, wA*hA);
   inicializarMatriz(h_B, wB*hB);

   int fin= (wB<tileRange) ? tileRange/wB : 1;

   float tiempoCPU=autoTuneNumHilos(&numHilos, h_A, h_B, h_C, wA, wB, hA, tileRange, fin);

   float tiempoAux=0.0;

   // Realizamos las operaciones pertinentes para cada dispositivo
   for(int i=0; i<numDispositivos; i++){

      error=cudaSetDevice(i);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al establecer dispositivo %d: %s\n", i, cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      // Reservamos memoria para los vectores en el dispositivo
      error=cudaMalloc((void **)&(d_A[i]), wA*hA*sizeof(float));

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al reservar memoria en el dispositivo %d: %s\n", i, cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      error=cudaMalloc((void **)&(d_B[i]), wB*hB*sizeof(float));

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al reservar memoria en el dispositivo %d: %s\n", i, cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      error=cudaMalloc((void **)&(d_C[i]), wB*hA*sizeof(float));

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al reservar memoria en el dispositivo %d: %s\n", i, cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      // Copiamos los vectores inicializados

      error=cudaEventCreate(&eventoInicio);
      if(error==cudaSuccess) error=cudaEventCreate(&eventoFin);
      if(error==cudaSuccess) error=cudaEventRecord(eventoInicio,0);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al tomar tiempo: %s\n", cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      error=cudaMemcpy(d_A[i], h_A, wA*hA*sizeof(float), cudaMemcpyHostToDevice);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al copiar matriz al dispositivo: %s\n", cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      error=cudaMemcpy(d_B[i], h_B, wB*hB*sizeof(float), cudaMemcpyHostToDevice);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al copiar matriz al dispositivo: %s\n", cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      error=cudaMemset(d_C[i], 0, wB*hA*sizeof(float));

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al rellenar la matriz del dispositivo con ceros: %s\n", cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      error=cudaEventRecord(eventoFin,0);
      if(error==cudaSuccess) error=cudaEventSynchronize(eventoFin);
      if(error==cudaSuccess) error=cudaEventElapsedTime(&tiempoTransferencia1, eventoInicio, eventoFin);
      if(error==cudaSuccess) error=cudaEventDestroy(eventoInicio);
      if(error==cudaSuccess) error=cudaEventDestroy(eventoFin);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al tomar tiempo: %s\n", cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      tiempoAux+=tiempoTransferencia1;
   }

   tiempoTransferencia1=tiempoAux;

   dim3 bloque(bsx, bsy, bsz);
   dim3 grid(gsx, gsy, gsz);
   size_t memCompartida= 2 * tileRange * tileRange * sizeof(float);

   autoTuneWorkLoad(&deviceWorkLoad, wlGPU, tiempoCPU, numDispositivos, grid, bloque, memCompartida, d_A, d_B, d_C, wA, wB, hA, tileRange, fin);

   const int finCPU=(1.0-deviceWorkLoad)*hA/tileRange;
   const int totalGPU=(hA+tileRange-1)/tileRange-finCPU;

   int* inicioGPU=(int*) malloc(numDispositivos*sizeof(int));
   int* finGPU=(int*) malloc(numDispositivos*sizeof(int));

   inicioGPU[0]=finCPU;
   finGPU[0]=finCPU+wlGPU[0]*totalGPU;

   for(int i=1; i<numDispositivos-1; i++){

      inicioGPU[i]=inicioGPU[i-1];
      finGPU[i]=inicioGPU[i]+wlGPU[i]*totalGPU;
   }

   if(numDispositivos!=1) inicioGPU[numDispositivos-1]=inicioGPU[numDispositivos-2];
   finGPU[numDispositivos-1]=(hA+tileRange-1)/tileRange;

   #if ONLY_TIME==0
      printf("Tamaño de las matrices: A(%d,%d) B(%d,%d)\n", wA, hA, wB, hB);
      printf("Tamaño de bloque: %d (%d, %d, %d)\n", bsx*bsy*bsz, bsx, bsy, bsz);
      printf("Tamaño de grid: %d (%d, %d, %d)\n", gsx*gsy*gsz, gsx, gsy, gsz);
      printf("Porcentaje de trabajo en GPU: %f%%\n", 100*deviceWorkLoad);

      printf("Ejecutando kernel\n");
   #endif

   error=cudaEventCreate(&eventoInicio);
   if(error==cudaSuccess) error=cudaEventCreate(&eventoFin);
   if(error==cudaSuccess) error=cudaEventRecord(eventoInicio,0);

   if(error!=cudaSuccess){

      fprintf(stderr, "Fallo al tomar tiempo: %s\n", cudaGetErrorString(error));
      finalizarAplicacion(EXIT_FAILURE);
   }

   for(int i=0; i<numDispositivos; i++){

      error=cudaSetDevice(i);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al establecer dispositivo %d: %s\n", i, cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      productoMatricialGPU<<<grid, bloque, memCompartida>>>(d_A[i], d_B[i], d_C[i], wA, wB, hA, tileRange, inicioGPU[i], finGPU[i]);
   }
   productoMatricialCPU(h_A, h_B, h_C, wA, wB, hA, tileRange, finCPU, numHilos);
   cudaThreadSynchronize();

   error = cudaGetLastError();

   if(error!=cudaSuccess){

      fprintf(stderr, "Fallo al lanzar el kernel: %s\n", cudaGetErrorString(error));
      finalizarAplicacion(EXIT_FAILURE);
   }

   error=cudaEventRecord(eventoFin,0);
   if(error==cudaSuccess) error=cudaEventSynchronize(eventoFin);
   if(error==cudaSuccess) error=cudaEventElapsedTime(&tiempoKernel, eventoInicio, eventoFin);
   if(error==cudaSuccess) error=cudaEventDestroy(eventoInicio);
   if(error==cudaSuccess) error=cudaEventDestroy(eventoFin);

   if(error!=cudaSuccess){

      fprintf(stderr, "Fallo al tomar tiempo: %s\n", cudaGetErrorString(error));
      finalizarAplicacion(EXIT_FAILURE);
   }

   tiempoAux=0.0;
   int sizeAux;

   for(int i=0; i<numDispositivos; i++){

      error=cudaSetDevice(i);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al establecer dispositivo %d: %s\n", i, cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      // Copiamos el vector C a la memoria del host

      error=cudaEventCreate(&eventoInicio);
      if(error==cudaSuccess) error=cudaEventCreate(&eventoFin);
      if(error==cudaSuccess) error=cudaEventRecord(eventoInicio,0);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al tomar tiempo: %s\n", cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      if(finGPU[i]==(hA+tileRange-1)/tileRange)
         sizeAux=wB*hA-inicioGPU[i]*tileRange*wB;
      else sizeAux=(finGPU[i]-inicioGPU[i])*tileRange*wB;

      error=cudaMemcpy(&h_C[inicioGPU[i]*tileRange*wB], &d_C[i][inicioGPU[i]*tileRange*wB], sizeAux*sizeof(float), cudaMemcpyDeviceToHost);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al copiar matriz al host: %s\n", cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      error=cudaEventRecord(eventoFin,0);
      if(error==cudaSuccess) error=cudaEventSynchronize(eventoFin);
      if(error==cudaSuccess) error=cudaEventElapsedTime(&tiempoTransferencia2, eventoInicio, eventoFin);
      if(error==cudaSuccess) error=cudaEventDestroy(eventoInicio);
      if(error==cudaSuccess) error=cudaEventDestroy(eventoFin);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al tomar tiempo: %s\n", cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      tiempoAux+=tiempoTransferencia2;

   }

   tiempoTransferencia2=tiempoAux;

   #if DEBUG
   // Verificamos el resultado
   comprobarMatriz(h_A, h_B, h_C, wA, wB, hA);
   #endif

   for(int i=0; i<numDispositivos; i++){

      error=cudaSetDevice(i);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al establecer dispositivo %d: %s\n", i, cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      // Liberamos la memoria del dispositivo

      error=cudaFree(d_A[i]);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al liberar memoria en el dispositivo %d: %s\n", i, cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      error=cudaFree(d_B[i]);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al liberar memoria en el dispositivo %d: %s\n", i, cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }

      error=cudaFree(d_C[i]);

      if(error!=cudaSuccess){

         fprintf(stderr, "Fallo al liberar memoria en el dispositivo %d: %s\n", i, cudaGetErrorString(error));
         finalizarAplicacion(EXIT_FAILURE);
      }
   }

   // Liberamos la memoria del host
   free(h_A); free(h_B); free(h_C);
   free(d_A); free(d_B); free(d_C);
   free(inicioGPU); free(finGPU);

   #if ONLY_TIME
      printf("wA %d hA %d wB %d hB %d workload %f ", wA, hA, wB, hB, deviceWorkLoad);
      printf("Tiempo total: %f ", tiempoKernel+tiempoTransferencia1+tiempoTransferencia2);
      printf("Tiempo de cálculo del kernel: %f ", tiempoKernel);
      printf("Tiempo de transferencias: %f (%f + %f) ", tiempoTransferencia1+tiempoTransferencia2, tiempoTransferencia1, tiempoTransferencia2);
      printf("hilosCPU %d ", numHilos);
      for(int i=0; i<numDispositivos; i++)
         printf("GPU %d workload %f ", i, wlGPU[i]);
      printf("\n");
   #else
      printf("Tiempo total: %f\n", tiempoKernel+tiempoTransferencia1+tiempoTransferencia2);
      printf("Tiempo de cálculo del kernel: %f\n", tiempoKernel);
      printf("Tiempo de transferencias: %f (%f + %f)\n", tiempoTransferencia1+tiempoTransferencia2, tiempoTransferencia1, tiempoTransferencia2);
   #endif

   free(wlGPU);

   finalizarAplicacion(EXIT_SUCCESS);
}

